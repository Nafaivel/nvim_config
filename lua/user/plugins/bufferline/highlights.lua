return {
  --[[ fill = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[ }, ]]
  --[[ background = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ tab = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ tab_selected = { ]]
  --[[   fg = tabline_sel_bg, ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ tab_close = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ close_button = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ close_button_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ close_button_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ buffer_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ buffer_selected = { ]]
  --[[   fg = normal_fg, ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ numbers = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[ }, ]]
  --[[ numbers_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[ }, ]]
  --[[ numbers_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ diagnostic = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[ }, ]]
  --[[ diagnostic_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[ }, ]]
  --[[ diagnostic_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ hint = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ hint_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ hint_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ hint_diagnostic = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ hint_diagnostic_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ hint_diagnostic_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ info = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ info_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ info_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ info_diagnostic = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ info_diagnostic_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ info_diagnostic_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ warning = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ warning_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ warning_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ warning_diagnostic = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ warning_diagnostic_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ warning_diagnostic_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = warning_diagnostic_fg ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ error = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ error_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  error_selected = {
    -- fg = '<colour-value-here>',
    bg = "Normal",
    -- sp = '<colour-value-here>'
    -- bold = true,
    -- italic = true,
  },
  --[[ error_diagnostic = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   sp = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ error_diagnostic_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  error_diagnostic_selected = {
    -- fg = '<colour-value-here>',
    bg = "Normal",
    -- sp = '<colour-value-here>',
    -- bold = true,
    -- italic = true,
  },
  --[[ modified = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ modified_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ modified_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ duplicate_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ duplicate_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[   italic = true ]]
  --[[ }, ]]
  --[[ duplicate = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[   italic = true ]]
  --[[ }, ]]
  --[[ separator_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ separator_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ separator = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ indicator_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>' ]]
  --[[ }, ]]
  --[[ pick_selected = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ pick_visible = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ pick = { ]]
  --[[   fg = '<colour-value-here>', ]]
  --[[   bg = '<colour-value-here>', ]]
  --[[   bold = true, ]]
  --[[   italic = true, ]]
  --[[ }, ]]
  --[[ offset_separator = { ]]
  --[[   fg = win_separator_fg, ]]
  --[[   bg = separator_background_color, ]]
  --[[ }, ]]
  fill = {
    fg = { attribute = "fg", highlight = "#ff0000" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  background = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  buffer_selected = {
    fg = { attribute = "fg", highlight = "#ff0000" },
    bg = { attribute = "bg", highlight = "#0000ff" },
  },
  buffer_visible = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  close_button = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  close_button_visible = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  -- close_button_selected = {
  --   fg = {attribute='fg',highlight='TabLineSel'},
  --   bg ={attribute='bg',highlight='TabLineSel'}
  --   },

  tab_selected = {
    fg = { attribute = "fg", highlight = "Normal" },
    bg = { attribute = "bg", highlight = "Normal" },
  },
  tab = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  tab_close = {
    -- fg = {attribute='fg',highlight='LspDiagnosticsDefaultError'},
    fg = { attribute = "fg", highlight = "TabLineSel" },
    bg = { attribute = "bg", highlight = "Normal" },
  },
  duplicate_selected = {
    fg = { attribute = "fg", highlight = "TabLineSel" },
    bg = { attribute = "bg", highlight = "TabLineSel" },
    underline = true,
  },
  duplicate_visible = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
    underline = true,
  },
  duplicate = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
    underline = true,
  },
  modified = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  modified_selected = {
    fg = { attribute = "fg", highlight = "Normal" },
    bg = { attribute = "bg", highlight = "Normal" },
  },
  modified_visible = {
    fg = { attribute = "fg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  separator = {
    fg = { attribute = "bg", highlight = "TabLine" },
    bg = { attribute = "bg", highlight = "TabLine" },
  },
  separator_selected = {
    fg = { attribute = "bg", highlight = "Normal" },
    bg = { attribute = "bg", highlight = "Normal" },
  },
  -- separator_visible = {
  --   fg = {attribute='bg',highlight='TabLine'},
  --   bg = {attribute='bg',highlight='TabLine'}
  --   },
  indicator_selected = {
    fg = { attribute = "fg", highlight = "LspDiagnosticsDefaultHint" },
    bg = { attribute = "bg", highlight = "Normal" },
  },
}
