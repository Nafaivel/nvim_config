vim.opt.background = "dark" -- set this to dark or light
local M = {}

M.CfgColorscheme = function()
  vim.cmd([[
try
  " let g:material_style = 'deep ocean'
  " colorscheme oxocarbon
  " colorscheme kanagawa
  " colorscheme material
  " colorscheme everblush
  " colorscheme catppuccin
  " colorscheme doom-one
  " colorscheme bloop-mirage
  " colorscheme ayu-mirage
  colorscheme ayu-dark
  " colorscheme monet
  " colorscheme bloop_nvim
  " colorscheme vim-monokai-tasty
  " colorscheme carbonfox
  " colorscheme nightfox
  " colorscheme dracula
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
  set background=dark
endtry
]])
end

-- Bg color
vim.cmd([[
  " hi Normal guibg=NONE ctermbg=NONE
  " hi Normal guibg=#101113
]])

-- Highlights
M.Hl = function()
  vim.cmd([[
  " md
  hi CodeBg            guibg=#1A1F26
  " dashboard
  hi DashboardHeader   guifg=#6495ED
  hi DashboardDesc     guifg=#7188A8
  hi DashboardKey      guifg=#EF3985
  hi DashboardIcon     guifg=#6495ED
  hi DashboardShortCut guifg=#6495ED

  " inlay hints
  hi LspInlayHint guifg=#A9A1E1 guibg=#1E1E2E
  ]])
end

M.CfgColorscheme()
M.Hl()
return M
