function open_nvim_cfg()
  vim.cmd("cd ~/.config/nvim")
  vim.cmd("e ~/.config/nvim/init.lua")
end

function Upd()
  require("lazy").sync()
  vim.cmd("TSUpdateSync")
  vim.cmd("MasonUpdateAll")
end

require("user.utils.spawn")

function OpenNvim()
  vim.fn.jobstart(os.getenv("TERMINAL") .. " -D " .. vim.fn.expand("%:p:h") .. " -e" .. " nvim " .. vim.fn.expand("%:p:f"), {detach=true})
end

--- @param path string
function OpenTerminal(path)
  if path == nil then
    path = vim.fn.expand("%:p:h")
  end
  vim.fn.jobstart(os.getenv("TERMINAL") .. " -D " .. path, {detach=true})
end

function set_desk(opts, desc)
  local new_opts = opts or {}
  new_opts.desc = desc
  return new_opts
end

-- -- may be oneday
-- ---@param char string
-- local function repace_char(char)
--
-- end
--
-- ---@param key string
-- local function map_key(key)
--   for char in key do
--     repace_char(char)
--   end
-- end
--
-- function MapNormalKeys()
--   local inspect = require("inspect")
--   print(vim.o.langmap)
--   for _, maps in ipairs(vim.api.nvim_get_keymap("n")) do
--     print(inspect(maps))
--   end
-- end
--
