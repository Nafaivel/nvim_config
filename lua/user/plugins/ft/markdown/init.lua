vim.g["vim_markdown_folding_level"] = 3
vim.g["vim_markdown_new_list_item_indent"] = 2

require("user.plugins.ft.markdown.nvim-markdown")
require("user.plugins.ft.markdown.markdown-composer")
require("user.plugins.ft.markdown.easytables")
require("user.plugins.ft.markdown.render")
