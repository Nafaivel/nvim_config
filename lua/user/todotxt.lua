function open_todo()
	vim.cmd([[
  cd ~/Documents/[shared]data/
  e ./todo.txt
  lua TodoSetup()
]])
end

local function keymaps()
  local opts = { noremap = true, silent = true }
  local keymap = vim.api.nvim_set_keymap
  keymap("n", "<leader>ts", ":lua require('todotxt').TodoSort()<cr>", opts)
end

function TodoSetup()
	vim.cmd([[lua require('todotxt').setup({})]])
  keymaps()
end

local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<leader>et", ":lua open_todo()<cr>", set_desk(opts, "[t]odo"))

--[[ vim.api.nvim_set_keymap("n", "<leader>ta", ":ToDoTxtCapture<cr>", {}) ]]
