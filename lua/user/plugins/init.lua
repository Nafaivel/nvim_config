require("user.plugins.lazy") -- package manager

-- dev features
require("user.plugins.conform")
require("user.plugins.treesitter")
require("user.plugins.luasnip")
require("user.plugins.dev.autopairs")
require("user.plugins.dev.ts-autotag") -- automatically close tags
require("user.plugins.dev.git")
require("user.plugins.comment")    -- comments on keymap
require("user.plugins.toggleterm") -- terminal
require("user.plugins.cmp")        -- completein engine
require("user.plugins.surround")   -- brackets on "ys" "ds" keymaps
require("user.plugins.projects")   -- projects
require("user.plugins.trouble")    -- error diagnostic for project
require("user.plugins.todo-comments")
require("user.plugins.rainbow-delimiters")

-- movement
require("user.plugins.telescope") -- in fact something like wofi, dmenu, rofi but for nvim
require("user.plugins.leap")      -- jump
require("user.plugins.harpoon")   -- buffer movement

-- ui stuff
require("user.plugins.bufferline") -- bufferline on top
require("user.plugins.lualine") -- statusline at bottom
require("user.plugins.dashboard")
require("user.plugins.which-key")
require("user.plugins.pretty-folds")
require("user.plugins.indent-blankline")
require("user.plugins.tyny-code-actions")
require("user.plugins.colorizer")

-- etc
require("user.plugins.translate") -- translate on keybind
require("user.plugins.undotree")  -- undo tree
require("user.plugins.better-spell") -- spellcheck

-- filetypes
require("user.plugins.ft.markdown") -- markdown setup
require("user.plugins.ft.org")      -- org setup
require("user.plugins.ft.vimtex")   -- latex helper
require("user.plugins.ft.yaml")     -- yaml
require("user.plugins.ft.rust")     -- rust

require("user.plugins.ollama")
