require("mason-nvim-dap").setup({
	automatic_setup = true,
})

-- TODO: fix dap handlers
--[[ require("mason-nvim-dap").setup_handlers({ ]]
--[[ 	function(source_name) ]]
--[[ 		-- all sources with no handler get passed here ]]
--[[ 		require("mason-nvim-dap.automatic_setup")(source_name) ]]
--[[ 	end, ]]
--[[ }) ]]

local mason_path = vim.env.HOME .. "/.local/share/nvim/mason"

-- overrided handlers
require("user.debug.handlers.rust").setup(mason_path)
require("user.debug.handlers.python").setup()

-- dap-ui
require("user.debug.dap-ui")
