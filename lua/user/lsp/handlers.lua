local M = {}

M.setup = function()
	local signs = {
		{ name = "DiagnosticSignError", text = "" },
		{ name = "DiagnosticSignWarn", text = "" },
		{ name = "DiagnosticSignHint", text = "" },
		{ name = "DiagnosticSignInfo", text = "" },
	}

	for _, sign in ipairs(signs) do
		vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
	end

	local config = {
		-- disable virtual text
		virtual_text = false,
		-- show signs
		signs = {
			active = signs,
		},
		update_in_insert = true,
		underline = true,
		severity_sort = true,
		float = {
			focusable = false,
			style = "minimal",
			border = "rounded",
			source = "always",
			header = "",
			prefix = "",
		},
	}

	vim.diagnostic.config(config)

	vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
		border = "rounded",
	})

	vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
		border = "rounded",
	})
end

local function lsp_highlight_document(client)
	-- Set autocommands conditional on server_capabilities
	if client.server_capabilities.document_highlight then
		vim.cmd(
			[[
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]],
			false
		)
	end
end

local function lsp_keymaps(bufnr)
	local opts = { noremap = true, silent = true, buffer = bufnr }
	local keymap = vim.keymap.set
	keymap("n", "gD", vim.lsp.buf.declaration, set_desk(opts, "[D]eclaration"))
	keymap("n", "K", vim.lsp.buf.hover, opts)
  keymap({ "n", "i" }, "<C-k>", vim.lsp.buf.signature_help, opts)
	keymap("n", "gdD", vim.lsp.buf.definition, set_desk(opts, "[d]efinition"))
  keymap("n", "gd", require("telescope.builtin").lsp_definitions, set_desk(opts, "[d]efinition"))
	keymap("n", "grR", vim.lsp.buf.references, set_desk(opts, "[r]eferences"))
  keymap("n", "gr", require("telescope.builtin").lsp_references,  set_desk(opts, "[r]eferences"))
	keymap("n", "giI", vim.lsp.buf.implementation, set_desk(opts, "[i]mplementation"))
  keymap("n", "gi", require("telescope.builtin").lsp_implementations,  set_desk(opts, "[i]mplementation"))
	keymap("n", "[e", function() vim.diagnostic.jump({ count = -1, border = "rounded", severity = vim.diagnostic.severity.ERROR}) end, set_desk(opts, "[e]rror"))
	keymap("n", "]e", function() vim.diagnostic.jump({ count = 1, border = "rounded", severity = vim.diagnostic.severity.ERROR}) end, set_desk(opts, "[e]rror"))
	keymap("n", "[d", function() vim.diagnostic.jump({ count = -1, border = "rounded", severity = vim.diagnostic.severity.WARN }) end, set_desk(opts, "[d]iagnostic"))
	keymap("n", "]d", function() vim.diagnostic.jump({ count = 1, border = "rounded", severity = vim.diagnostic.severity.WARN}) end, set_desk(opts, "[d]iagnostic"))
	keymap("n", "gl", function() vim.diagnostic.open_float({ border = "rounded" }) end, set_desk(opts, "f[l]oating diagnostic"))
	-- keymap("n", "<leader>le", vim.diagnostic.setloclist, set_desk(opts, "[e]rrors"))
	keymap("n", "<leader>lI", ":LspInfo<CR>", set_desk(opts, "lsp [I]fo"))
	keymap("n", "<leader>li", function () vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())end, set_desk(opts, "toggle [i]nlay hints"))
	-- keymap({"n", "v"}, "<leader>la", vim.lsp.buf.code_action, set_desk(opts, "code [a]ction"))
	keymap({"n", "v"}, "<leader>la", require("tiny-code-action").code_action, set_desk(opts, "code [a]ction"))
  local ok, telescope = pcall(require, "telescope.builtin")
  if ok then
    keymap("n", "<leader>lj", function() telescope.treesitter() end, set_desk(opts, "[s]ymbols"))
    keymap("n", "<leader>lJ", function() telescope.lsp_dynamic_workspace_symbols() end, set_desk(opts, "workspace [S]ymbols"))
  end
end

M.on_attach = function(client, bufnr)
	if client.name == "tsserver" then
		client.resolved_capabilities.document_formatting = false
	end
	lsp_keymaps(bufnr)
	lsp_highlight_document(client)
end

local capabilities = vim.lsp.protocol.make_client_capabilities()

local status_ok, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
if not status_ok then
	return
end

M.capabilities = cmp_nvim_lsp.default_capabilities(capabilities)

return M
