-- HACK: to bypass some extensions which change this (I suspect markdown.nvim(render))
vim.defer_fn(function()
  vim.opt.foldlevel = 99
  vim.opt.foldmethod = "expr"
  vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
end, 1000)

local function save_current_buffer_to_history()
  -- to get normal filename i should get content of third line starting from third char if it's gen.nvim and replace all spaces with underscores
  local filename = os.date("%Y.%m.%d_%H%M").."_from_nvim.md"
  local path2file = "~/Documents/.gpt/"..filename
  vim.cmd("w ".. path2file)
end

local keymap = vim.keymap.set
local opts = { silent = true, noremap = true }

keymap({ "n", "v" }, "<leader>Os", save_current_buffer_to_history, set_desk(opts, "[s]ave to history"))
