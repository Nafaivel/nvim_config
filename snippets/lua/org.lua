local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
-- local sn = ls.snippet_node
local c = ls.choice_node
-- local d = ls.dynamic_node
-- local l = require("luasnip.extras").lambda
-- local r = require("luasnip.extras").rep
-- local p = require("luasnip.extras").partial
-- local m = require("luasnip.extras").match
-- local n = require("luasnip.extras").nonempty
-- local dl = require("luasnip.extras").dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local types = require("luasnip.util.types")
-- local conds = require("luasnip.extras.expand_conditions")
-- local events = require("luasnip.util.events")

local function clear_buffer()
  -- vim.api.nvim_buf_delete(0, {force=true})
  vim.api.nvim_buf_set_lines(0, 1, 2, true, { "" })
  vim.defer_fn(function()
    vim.cmd("normal dd")
  end, 100)
  -- vim.defer_fn(function()
  --   ls.jump(1)
  -- end, 150)
end

--- @return string|osdate
local function get_date()
  return os.date("[%Y-%m-%d %a %H:%M]")
end

local todo_states = function(n)
  return c(n, {
    t("TODO"),
    t("NOW"),
    t("WAFO"),
    t("SOMD"),
  })
end

return {
  s({ trig = "ct", describe = "capture todo" }, {
    f(clear_buffer),
    i(1),
    t({ "", "", "* " }),
    todo_states(2),
    t(" "),
    i(3),
    t({ "", ":LOGBOOK:", '- State "TODO" from ' }),
    f(get_date),
    t({ "", ":END:", "" }),
    i(0),
  }),
  s(
    "t",
    fmt(
      [[
        * {} {}
        :LOGBOOK:
        - State "TODO" from {}
        :END:
      ]],
      {
        todo_states(1),
        i(2),
        f(get_date),
      }
    )
  ),
}
