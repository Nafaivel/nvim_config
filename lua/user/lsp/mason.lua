local status_ok, mason = pcall(require, "mason")
if not status_ok then
  return
end

mason.setup({
  ui = {
    icons = {
      package_installed = "✓",
      package_pending = "➜",
      package_uninstalled = "✗",
    },
  },
})

local status_ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not status_ok then
  return
end

local opts = {
  on_attach = require("user.lsp.handlers").on_attach,
  capabilities = require("user.lsp.handlers").capabilities,
}

mason_lspconfig.setup({
  ensure_installed = { "lua_ls" },
})

local lspconfig = require("lspconfig")

mason_lspconfig.setup_handlers({
  -- default server
  function(server_name) -- default handler (optional)
    require("lspconfig")[server_name].setup(opts)
  end,
  -- overrided servers
  ["rust_analyzer"] = function()
    -- require("user.lsp.settings.rustaceanvim")
    -- local rt = require("user.lsp.settings._rust-tools")
    -- rt.setup(opts)
  end,
  ["lua_ls"] = function()
    local lua_ls_opts = require("user.lsp.settings.lua_ls")
    opts = vim.tbl_deep_extend("force", lua_ls_opts, opts)
    lspconfig.lua_ls.setup(opts)
  end,
  ["gopls"] = function()
    lspconfig.gopls.setup(opts)
  end,
  ["ruff"] = function()
    opts = vim.tbl_deep_extend("force", opts, { init_options = { settings = { args = {} } } })
    lspconfig.ruff.setup(opts)
  end,
  ["basedpyright"] = function()
    local basedpyright_opts = require("user.lsp.settings.basedpyright")
    opts = vim.tbl_deep_extend("force", basedpyright_opts, opts)
    lspconfig.basedpyright.setup(opts)
  end,
  ["pyright"] = function()
    local pyright_opts = require("user.lsp.settings.pyright")
    opts = vim.tbl_deep_extend("force", pyright_opts, opts)
    lspconfig.pyright.setup(opts)
  end,
})

require("mason-update-all").setup()
