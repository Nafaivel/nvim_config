local ls = require("luasnip")
local i = ls.insert_node
local s = ls.snippet
local fmt = require("luasnip.extras.fmt").fmt

return {
  s(
    "new",
    fmt(
    [[
      pub fn new({}) -> Self {{
          Self {{{}}}
      }}
    ]],
      { i(1, ""), i(2, "") }
    )
  ),
}
