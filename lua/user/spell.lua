-- spell check
vim.cmd[[
function! ToggleSpell()
    if !exists("b:spell")
        setlocal spell spelllang=en_us,ru
        let b:spell = 1
    else
        setlocal nospell
        unlet b:spell
    endif
endfunction
 
nmap <F4> :call ToggleSpell()<CR>
imap <F4> <Esc>:call ToggleSpell()<CR>a
]]
--
-- Shorten function name
local opts = { noremap = true, silent = true }
local keymap = vim.api.nvim_set_keymap

keymap("n", "<leader>ss", ":call ToggleSpell()<CR>", set_desk(opts, "[s]pell"))
