vim.cmd([[
fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
call SetupCommandAlias("ц","w")
call SetupCommandAlias("й","q")
call SetupCommandAlias("цй","wq")
call SetupCommandAlias("git","Git")
]])
