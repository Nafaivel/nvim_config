local ok, tl = pcall(require, "translate")
if not ok then
  return
end

tl.setup({
  default = {
    output = "replace",
    append = true,
    parse_before = "trim",
    parse_after = "rate",
  },
})

local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }
keymap({'n', 'v'}, "<leader>tt", ":Translate be<cr>", set_desk(opts, "be"))
keymap({'n', 'v'}, "<leader>tT", ":Translate be -output=floating<cr>", set_desk(opts, "be floating"))
keymap({'n', 'v'}, "<leader>tp", ":Translate be -output=floating -command=translate_shell<cr>", set_desk(opts, "be floating"))
keymap({'n', 'v'}, "<leader>te", ":Translate en<cr>", set_desk(opts, "[e]n"))
keymap({'n', 'v'}, "<leader>tE", ":Translate en -output=floating<cr>", set_desk(opts, "[E]n floating"))
