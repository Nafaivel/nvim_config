return {
  settings = {
    basedpyright = {
      typeCheckingMode = "standard",
      -- typeCheckingMode = "strict",
      analysis = {
        autoSearchPaths = true,
        diagnosticMode = "openFilesOnly",
        useLibraryCodeForTypes = true,
        reportMissingTypeStubs = false,

        reportUnknownParameterType = "info",
        reportUnknownArgumentType = "info",
        reportUnknownLambdaType = "info",
        reportUnknownVariableType = "info",
        reportUnknownMemberType = "info",
      },
    },
  },
}
