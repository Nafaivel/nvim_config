local status_ok, telescope = pcall(require, "telescope")
if not status_ok then
  return
end

local actions = require("telescope.actions")
local builtin = require("telescope.builtin")

telescope.setup({
  pickers = {
    buffers = {
      mappings = {
        n = {
          ["dd"] = actions.delete_buffer,
        },
        i = {
          ["<c-d>"] = actions.delete_buffer,
        },
      },
    },
  },
  defaults = {
    layout_config = {
      -- vertical = {
      --   width = 0.95
      -- },
      -- horizontal = {
      --   width = 0.95
      -- },
    },
    path_display = {
      "smart",
    },
    mappings = {
      i = {
        ["<C-j>"] = actions.move_selection_next,
        ["<C-k>"] = actions.move_selection_previous,
        ["<C-l>"] = actions.select_default,
        ["<C-h>"] = actions.close,
      },
    },
    file_ignore_patterns = {
      -- images
      "%.png",
      "%.jpg",
      "%.jpeg",
      "%.svg",
      "%.webp",
      "%.avif",
      "%.gif",
      "%.bmp",
      -- dev
      "node_modules",
      "%.git/",
      ".cache",
      "%.o",
      "%.a",
      "%.out",
      "%.class",
      "LICENSE",
      -- videos
      "%.mkv",
      "%.mp4",
      -- archives
      "%.zip",
    },
  },
  extensions = {
    project = {
      hidden_files = false, -- default: false
      order_by = "recent",
      search_by = "title",
      sync_with_nvim_tree = true, -- default false
    },
    file_browser = {
      -- theme = "ivy",
      -- disables netrw and use telescope-file-browser in its place
      path = "%:p:h",
      hijack_netrw = false,
      follow_symlinks = true,
      hidden = true,
      select_buffer = true,
      mappings = {},
    },
  },
})

telescope.load_extension("fzf")
-- telescope.load_extension("projects")
telescope.load_extension("project")
telescope.load_extension("frecency")
telescope.load_extension("recent_files")
telescope.load_extension("file_browser")
telescope.load_extension("nerdy")
telescope.load_extension("heading")

local function search_in_buffer()
  require("telescope.builtin").live_grep({ search_dirs = { vim.fn.expand("%:p") } })
end

local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

keymap("n", "<leader>lj", ":Telescope heading<CR>", set_desk(opts, "[j]ump to heading")) -- this should be overriden with lsp keybinding
keymap("n", "<leader>ff", ":Telescope find_files<CR>", set_desk(opts, "[f]iles"))
keymap("n", "<leader>fF", ":Telescope frecency workspace=CWD<CR>", set_desk(opts, "[F]recency"))
keymap("n", "<leader>fq", telescope.extensions.frecency.frecency, set_desk(opts, "fre[q]uency"))
keymap("n", "<leader>fr", ":Telescope recent_files pick<CR>", set_desk(opts, "[r]ecent"))
keymap("n", "<leader>fp", ":Telescope project<cr>", set_desk(opts, "[p]rojects"))
keymap("n", "<leader>fg", ":Telescope live_grep <CR>", set_desk(opts, "[g]rep"))
keymap("n", "<leader>fG", search_in_buffer, set_desk(opts, "[G]rep this file"))
keymap("n", "<C-s>", search_in_buffer, set_desk(opts, "[G]rep this file")) -- mimic emacs <C-s key>
keymap("n", "<leader>fc", ":Telescope colorscheme<CR>", set_desk(opts, "[c]olorscheme"))
keymap("n", "<leader>fh", ":Telescope help_tags<CR>", set_desk(opts, "[h]elp"))
keymap("n", "<leader>fk", ":Telescope keymaps<CR>", set_desk(opts, "[k]ey"))
keymap("n", "<leader>fn", telescope.extensions.nerdy.nerdy, set_desk(opts, "[n]erd font"))
keymap("n", "<leader>f<CR>", builtin.resume, set_desk(opts, "reusme"))
keymap("n", "<leader>R", ":Telescope file_browser<CR>", set_desk(opts, "file b[r]owser"))
keymap("n", "<leader>b", ":Telescope buffers<CR>", set_desk(opts, "[b]uffers"))
