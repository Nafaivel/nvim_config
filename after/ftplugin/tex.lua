vim.g['tex_flavor'] = 'latex'
vim.g['vimtex_view_method'] = 'zathura'
vim.g['vimtex_quickfix_mode'] = 0
--[[ vim.cmd("set conceallevel = 1") ]]
vim.g['tex_conceal'] = 'abdmg'


local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

keymap("n", "<leader>lp", ":VimtexView<CR>", set_desk(opts, "[p]review"))
keymap("n", "<leader>lc", ":VimtexCompile<CR>", set_desk(opts, "[c]ompile"))
