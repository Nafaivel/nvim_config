-- local ok, ollama = pcall(require, "ollama")
local ok, gen = pcall(require, "gen")

if not ok then
  return
end

gen.setup({
  model = "llama3.1", -- The default model to use.
  host = "localhost", -- The host running the Ollama service.
  port = "11434", -- The port on which the Ollama service is listening.
  quit_map = "q", -- set keymap for close the response window
  retry_map = "<c-r>", -- set keymap to re-send the current prompt
  init = function(options)
    pcall(io.popen, "ollama serve > /dev/null 2>&1 &")
  end,
  -- Function to initialize Ollama
  command = function(options)
    local body = { model = options.model, stream = true }
    return "curl --silent --no-buffer -X POST http://" .. options.host .. ":" .. options.port .. "/api/chat -d $body"
  end,
  -- The command for the Ollama service. You can use placeholders $prompt, $model and $body (shellescaped).
  -- This can also be a command string.
  -- The executed command must return a JSON object with { response, context }
  -- (context property is optional).
  -- list_models = '<omitted lua function>', -- Retrieves a list of model names
  display_mode = "split", -- The display mode. Can be "float" or "split".
  show_prompt = true, -- Shows the prompt submitted to Ollama.
  show_model = false, -- Displays which model you are using at the beginning of your chat session.
  no_auto_close = false, -- Never closes the window automatically.
  debug = false -- Prints errors and the command which is run.
})

gen.prompts["Ask"] = nil
gen.prompts["Change"] = nil
gen.prompts["Change_Code"] = nil
gen.prompts["Chat"] = nil
gen.prompts["Generate"] = nil

local my_prompts = {
  Spytać_pra_kod = {
    prompt = "$input:\n```$filetype\n$text\n```",
    replace = false
  },
  Vyłučanaje_z_promptam = {
    prompt = "$input:\n$text",
    replace = false
  },
  Spytać_z_karotkim_adkazam = {
    prompt = "$input. Be SHORT but informative enough to understand, please.",
    replace = false
  },
  Spytać = {
    prompt = "$input",
    replace = false
  },
  Vyłučanaje = {
    prompt = "$text",
    replace = false
  },
  Palepš_input_uncens = {
    prompt = "enhance spelling (find mistakes and show me how to fix or say that text fully correct, also don't show me original text only corrected version and places of correction without anything else(even preamble like 'here is correction')) of text in following format:\nfixed_version_of_text\n\nlist of fixed\n\nfor this text:\n$text",
    replace = false,
    model = "llama2-uncensored"
  },
  Palepš_input = {
    prompt = "enhance spelling (find mistakes and show me how to fix or say that text fully correct, also don't show me original text only corrected version and places of correction without anything else(even preamble like 'here is correction')) of text in following format:\nfixed_version_of_text\n\nlist of fixed\n\nfor this text:\n$text",
    replace = false,
  },
  Zamiani_vyłučanaje_z_promptam={
    prompt = "$input:\n$text",
    replace = true
  }
}

gen.prompts = vim.tbl_extend("force", my_prompts, gen.prompts)

local keymap = vim.keymap.set
local opts = { silent = true, noremap = true }

keymap({ "n", "v" }, "<leader>oo", ":Gen<cr>", set_desk(opts, "[o]prompt"))
keymap({ "n", "v" }, "<leader>Oo", ":Gen<cr>", set_desk(opts, "[o]prompt"))
keymap({ "n", "v" }, "<leader>OO", ":Gen<cr>", set_desk(opts, "[o]prompt"))
keymap({ "n", "v" }, "<leader>Oh", ":Lf ~/Documents/.gpt/<cr>", set_desk(opts, "[h]istory"))
