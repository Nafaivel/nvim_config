return {
  settings = {
    python = {
      analysis = {
        typeCheckingModetypeCheckingMode = "off",
      },
      linting = {
        pylintEnabled = true,
        banditEnabled = true
      }
    },
  },
}
