-- local ok, pf = pcall(require, "pretty-fold")
--
-- if not ok then
--   return
-- end
--
-- local opts = {
--   custom_function_arg = "Hello from inside custom function!",
--   sections = {
--     left = {
--       function(config)
--         return config.custom_function_arg
--       end,
--     },
--   },
--   -- sections = {
--   --     left = {
--   --        'content',
--   --     },
--   --     right = {
--   --        ' ', 'number_of_folded_lines', ': ', 'percentage', ' ',
--   --        function(config) return config.fill_char:rep(3) end
--   --     }
--   --  },
--   fill_char = "•",
--
--   remove_fold_markers = true,
--
--   -- Keep the indentation of the content of the fold string.
--   keep_indentation = true,
--
--   -- Possible values:
--   -- "delete" : Delete all comment signs from the fold string.
--   -- "spaces" : Replace all comment signs with equal number of spaces.
--   -- false    : Do nothing with comment signs.
--   process_comment_signs = "spaces",
--
--   -- Comment signs additional to the value of `&commentstring` option.
--   comment_signs = {},
--
--   -- List of patterns that will be removed from content foldtext section.
--   stop_words = {
--     "@brief%s*", -- (for C++) Remove '@brief' and all spaces after.
--   },
--
--   add_close_pattern = true, -- true, 'last_line' or false
--
--   matchup_patterns = {
--     { "{",  "}" },
--     { "%(", ")" }, -- % to escape lua pattern char
--     { "%[", "]" }, -- % to escape lua pattern char
--   },
--
--   ft_ignore = { "neorg" },
-- }
-- pf.setup(opts)

-- local global_setup = {
--   sections = {
--     left = { "content" },
--     right = {
--       " ",
--       function()
--         return ("[%dL]"):format(vim.v.foldend - vim.v.foldstart)
--       end,
--       "[",
--       "percentage",
--       "]",
--     },
--   },
--   matchup_patterns = {
--     { "{",  "}" },
--     { "%(", ")" }, -- % to escape lua pattern char
--     { "%[", "]" }, -- % to escape lua pattern char
--   },
--   -- add_close_pattern = true,
--   process_comment_signs = ({ "delete", "spaces", false })[2],
-- }
--
-- local function ft_setup(lang, options) -- {{{
--   local opts = vim.tbl_deep_extend("force", global_setup, options)
--   -- combine global and ft specific matchup_patterns
--   if opts and opts.matchup_patterns and global_setup.matchup_patterns then
--     opts.matchup_patterns = vim.list_extend(opts.matchup_patterns, global_setup.matchup_patterns)
--   end
--   require("pretty-fold").ft_setup(lang, opts)
-- end -- }}}
--
-- require("pretty-fold").setup(global_setup)
--
-- ft_setup("lua", {                -- {{{
--   matchup_patterns = {
--     { "^%s*do$",          "end" }, -- do ... end blocks
--     { "^%s*if",           "end" }, -- if ... end
--     { "^%s*for",          "end" }, -- for
--     { "function[^%(]*%(", "end" }, -- 'function( or 'function (''
--   },
-- })                               -- }}}
--
-- ft_setup("vim", {                -- {{{
--   matchup_patterns = {
--     { "^%s*function!?[^%(]*%(", "endfunction" },
--   },
-- }) -- }}}
