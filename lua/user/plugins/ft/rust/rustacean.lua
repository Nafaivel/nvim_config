local handler = require("user.lsp.handlers")

vim.g.rustaceanvim = {
  inlay_hints = {
    auto = true,
    only_current_line = false,
    show_parameter_hints = true,
    parameter_hints_prefix = "<- ",
    other_hints_prefix = "=> ",
    max_len_align = false,
    max_len_align_padding = 1,
    right_align = false,
    right_align_padding = 7,
    highlight = "Comment",
  },
  -- Plugin configuration
  tools = {
    hover_actions = {
      auto_focus = true,
    },
  },
  -- LSP configuration
  server = {
    on_attach = function(client, bufnr)
      handler.setup()
      handler.on_attach(client, bufnr)

      local ok, wk = pcall(require, "which-key")
      if ok then
        wk.add({ "<leader>ll", group = "[l]sp specific" })
      end

      local opts = { silent = true, buffer = bufnr }
      vim.keymap.set("n", "<leader>lq",  ":RustAnalyzer restart<cr>", set_desk(opts, "[q]reload workspace"))
      vim.keymap.set("n", "<leader>lld", ":RustLsp openDocs<cr>", set_desk(opts, "[d]ocs under cursor"))
      vim.keymap.set("n", "<leader>llc", ":RustLsp openCargo<cr>", set_desk(opts, "[c]argo.toml"))
      vim.keymap.set("n", "<leader>lll", ":RustLsp explainError<cr>", set_desk(opts, "exp[l]ain"))
      vim.keymap.set("n", "<leader>lle", ":RustLsp expandMacro<cr>", set_desk(opts, "[e]xpand macro"))
      vim.keymap.set("n", "<leader>llt", ":RustLsp testables<cr>", set_desk(opts, "[t]est"))
      -- vim.lsp.inlay_hint.enable(true)
    end,
    -- default_settings = {
    ["rust-analyzer"] = {
      imports = {
        granularity = {
          group = "module",
        },
        prefix = "self",
      },
      check = {
        command = "clippy",
      },
      cargo = {
        buildScripts = {
          enable = true,
        },
      },
      procMacro = {
        enable = true,
      },
      diagnostics = {
        enable = true,
        experimental = {
          enable = true
        }
      },
      completion = {
        autoimport = {
          enable = true,
        },
      },
      -- },
    },
  },
  -- DAP configuration
  dap = {},
}

-- -- Create an autocommand group
-- vim.api.nvim_create_augroup("RustFmt", { clear = true })
--
-- -- Define the autocommand
-- vim.api.nvim_create_autocmd("BufWritePre", {
--   group = "RustFmt",
--   pattern = "*.rs",
--   callback = function()
--     vim.lsp.buf.format()
--   end,
-- })

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldlevel = 99
