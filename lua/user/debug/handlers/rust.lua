local M = {}
local function setup(mason_path)
	local dap = require("dap")

	local cfg = dap.configurations
	local adapter = dap.adapters

	local extension_path = mason_path .. "/packages/codelldb/extension/"
	local codelldb_path = extension_path .. "adapter/codelldb"
	--[[ local liblldb_path = extension_path .. "lldb/lib/liblldb.so" ]]

	--[[ require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path) ]]

	adapter.codelldb = {
		type = "server",
		host = "127.0.0.1",
		port = "${port}", -- 💀 Use the port printed out or specified with `--port`
		executable = {
			command = codelldb_path, -- adjust as needed, must be absolute path
			args = { "--port", "${port}" },
		},
	}

	cfg.rust = {
		{
			name = "Launch file",
			type = "codelldb",
			request = "launch",
			program = function()
				return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/target/debug/", "file")
			end,
			cwd = "${workspaceFolder}",
			stopOnEntry = true,
		},
	}
end

M.setup = setup

return M
