local status_ok, lf = pcall(require, "lf")
if not status_ok then
  return
end

lf.setup({
	escape_quit = false,
  -- default_file_manager = true, -- make lf default file manager
	border = "rounded",
})
