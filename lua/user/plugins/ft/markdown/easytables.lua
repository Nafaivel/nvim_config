local ok, easytables = pcall(require, "easytables")

if not ok then
  return
end

local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

keymap("n", "<leader>me", ":EasyTablesImportThisTable<cr>", set_desk(opts, "[e]dit table"))
keymap("n", "<leader>mc", ":EasyTablesCreateNew 2 2<cr>", set_desk(opts, "[c]reate table"))

easytables.setup({
  table = {
    -- Whether to enable the header by default
    header_enabled_by_default = true,
    window = {
      preview_title = "Table Preview",
      prompt_title = "Cell content",
      -- Either "auto" to automatically size the window, or a string
      -- in the format of "<width>x<height>" (e.g. "20x10")
      size = "auto",
    },
    cell = {
      -- Min width of a cell (excluding padding)
      min_width = 3,
      -- Filler character for empty cells
      filler = " ",
      align = "left",
    },
    -- Characters used to draw the table
    -- Do not worry about multibyte characters, they are handled correctly
    border = {
      top_left = "┌",
      top_right = "┐",
      bottom_left = "└",
      bottom_right = "┘",
      horizontal = "─",
      vertical = "│",
      left_t = "├",
      right_t = "┤",
      top_t = "┬",
      bottom_t = "┴",
      cross = "┼",
      header_left_t = "╞",
      header_right_t = "╡",
      header_bottom_t = "╧",
      header_cross = "╪",
      header_horizontal = "═",
    },
  },
  export = {
    markdown = {
      -- Padding around the cell content, applied BOTH left AND right
      -- E.g: padding = 1, content = "foo" -> " foo "
      padding = 1,
      -- What markdown characters are used for the export, you probably
      -- don't want to change these
      characters = {
        horizontal = "-",
        vertical = "|",
        -- Filler for padding
        filler = " ",
      },
    },
  },
  set_mappings = function(buf)
    local keymap = vim.api.nvim_buf_set_keymap
    keymap(buf, "n", "q", ":q<cr>", {})
    keymap(buf, "n", "<C-n>", ":q<cr>", {})
    keymap(buf, "n", "<C-y>", ":ExportTable<cr>", {})

    keymap(buf, "n", "D", ":DeleteRow<cr>", {})
    keymap(buf, "n", "gD", ":DeleteColumn<cr>", {})

    keymap(buf, "n", "o", ":InsertRowBelow<cr>", {})
    keymap(buf, "n", "O", ":InsertRowAbove<cr>", {})
    keymap(buf, "n", "go", ":InsertColumnLeft<cr>", {})
    keymap(buf, "n", "gO", ":InsertColumnRight<cr>", {})

    -- keymap(buf, "n", "h", ":JumpLeft<CR>", {})
    -- keymap(buf, "n", "l", ":JumpRight<CR>", {})
    -- keymap(buf, "n", "k", ":JumpUp<CR>", {})
    -- keymap(buf, "n", "j", ":JumpDown<CR>", {})

    keymap(buf, "i", "<C-h>", "<Esc>:JumpLeft<CR>i", {})
    keymap(buf, "i", "<C-l>", "<Esc>:JumpRight<CR>i", {})
    keymap(buf, "i", "<C-k>", "<Esc>:JumpUp<CR>i", {})
    keymap(buf, "i", "<C-j>", "<Esc>:JumpDown<CR>i", {})

    keymap(buf, "n", "<C-h>", ":JumpLeft<CR>", {})
    keymap(buf, "n", "<C-l>", ":JumpRight<CR>", {})
    keymap(buf, "n", "<C-k>", ":JumpUp<CR>", {})
    keymap(buf, "n", "<C-j>", ":JumpDown<CR>", {})

    keymap(buf, "n", "<S-h>", ":SwapWithLeftCell<CR>", {})
    keymap(buf, "n", "<S-l>", ":SwapWithRightCell<CR>", {})
    keymap(buf, "n", "<S-k>", ":SwapWithUpperCell<CR>", {})
    keymap(buf, "n", "<S-j>", ":SwapWithLowerCell<CR>", {})

    keymap(buf, "n", "<C-S-h>", ":SwapWithLeftColumn<CR>", {})
    keymap(buf, "n", "<C-S-l>", ":SwapWithRightColumn<CR>", {})
    keymap(buf, "n", "<C-S-k>", ":SwapWithUpperRow<CR>", {})
    keymap(buf, "n", "<C-S-j>", ":SwapWithLowerRow<CR>", {})

    keymap(buf, "n", "<Tab>", ":JumpToNextCell<CR>", {})
    keymap(buf, "n", "<S-Tab>", ":JumpToPreviousCell<CR>", {})

    vim.cmd(
        string.format(
            "autocmd BufWriteCmd <buffer=%s> ExportTable",
            0 -- current buffer should be fine
        )
    )
  end,
})
