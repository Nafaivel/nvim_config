local ok, nc = pcall(require, "neoconf")
if not ok then
  return
end

nc.setup()

local ok, venom = pcall(require, "venom")
if ok then
  venom.setup({ symbol = "" })
end
