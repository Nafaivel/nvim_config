local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
--[[ local keymap = vim.api.nvim_set_keymap ]]
local keymap = vim.keymap.set
-- local kdel = vim.keymap.del

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window resize
keymap("n", "<A-h>", ":vertical resize +2<CR>", opts)
keymap("n", "<A-j>", ":resize -2<CR>", opts)
keymap("n", "<A-k>", ":resize +2<CR>", opts)
keymap("n", "<A-l>", ":vertical resize -2<CR>", opts)

-- cd dir of current file
keymap("n", "<leader>cd", ":cd %:p:h<cr>:pwd<cr>", set_desk(opts, "[c]d"))
keymap("n", "<leader>..", ":cd ..<cr>:pwd<cr>", set_desk(opts, "[.]."))
keymap("n", "<leader>.e", ":e ./.env<cr>", set_desk(opts, ".[e]nv"))
keymap("n", "<leader>.E", ":e ./.env.example<cr>", set_desk(opts, ".[E]nv.example"))
-- quick close buffer
keymap("n", "<leader>d", ":Bdelete<cr>", set_desk(opts, "[d]el buff"))
keymap("n", "<leader>D", ":Bdelete!<cr>", set_desk(opts, "force [D]el buff"))

-- ArgWrap
keymap("n", "<leader>a", ":ArgWrap<CR>", set_desk(opts, "[a]rgwrap"))

-- Tab move
-- keymap("n", "<TAB>", ":tabnext<cr>", opts)
-- keymap("n", "<S-TAB>", ":tabNext<cr>", opts)

-- Disable search hilighting
keymap("n", "<leader>/", ":noh <cr>", set_desk(opts, "noh"))
keymap("n", "<Esc>", "<cmd>nohlsearch<CR>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers it remaps in BufferLine
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Insert --
-- Press jk fast to enter
-- keymap("i", "jk", "<ESC>", opts)
keymap("i", "<C-A>", "<C-O>yiW<End>=<C-R>=<C-R>0<CR>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)
--
-- clipboard copy
keymap("v", "<C-c>", '"+y', opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
-- keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
-- keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- Terminal --
-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

-- replace gj to j, gk to k
keymap("n", "gj", "v:count ? 'gj' : 'j'", { noremap = true, expr = true })
keymap("n", "gk", "v:count ? 'gk' : 'k'", { noremap = true, expr = true })
keymap("n", "j", "v:count ? 'j' : 'gj'", { noremap = true, expr = true })
keymap("n", "k", "v:count ? 'k' : 'gk'", { noremap = true, expr = true })

-- Plugins
-- dashboard
keymap("n", "<leader>B", ":Dashboard<cr>", set_desk(opts, "dash[B]oard"))
-- open terninal
keymap("n", "<leader>gt", ":lua OpenTerminal()<CR>", set_desk(opts, "[t]erninal"))
keymap("n", "<leader>gT", ":lua OpenTerminal(vim.fn.getcwd())<CR>", set_desk(opts, "[T]erninal at nvim cd"))
keymap("n", "<leader>gn", ":lua OpenNvim()<CR>", set_desk(opts, "[n]vim"))
-- lf
keymap("n", "<leader>r", ":lua require('lf').start()<CR>", set_desk(opts, "[r]lf"))
-- set / source
keymap("n", "<leader>sc", ":luafile ~/.config/nvim/init.lua<cr>", set_desk(opts, "[c]onfig"))
keymap("n", "<leader>st", ":set wrap!<cr>", set_desk(opts, "[t]runcate"))
keymap("n", "<leader>sF", ":set foldlevel=99<cr>", set_desk(opts, "[F]oldlvl99"))
keymap("n", "<leader>sT4", ":set tabstop=4<cr>", set_desk(opts, "[T]ab 4"))
keymap("n", "<leader>sT2", ":set tabstop=2<cr>", set_desk(opts, "[T]ab 2"))
keymap("n", "<leader>sfm", ":set filetype=markdown<cr>", set_desk(opts, "[m]arkdown"))
keymap("n", "<leader>sfh", ":set filetype=html<cr>", set_desk(opts, "[h]tml"))
keymap("n", "<leader>sfc", ":set filetype=css<cr>", set_desk(opts, "[c]ss"))
keymap("n", "<leader>sfj", ":set filetype=json5<cr>", set_desk(opts, "[j]son5"))
keymap("n", "<leader>sfJ", ":set filetype=json<cr>", set_desk(opts, "[J]son"))
keymap("n", "<leader>sfo", ":set filetype=org<cr>", set_desk(opts, "[o]rg"))
keymap("n", "<leader>sfd", ":filetype detect<cr>", set_desk(opts, "re[d]etect"))
-- go edit section
keymap("n", "<leader>ec", open_nvim_cfg, set_desk(opts, "[c]onfig"))
--[[ keymap("n", "<leader>eC", ":Lf ~/.config/<cr>", opts) ]]
--[[ keymap("n", "<leader>eno", ":Lf ~/Documents/notes/<cr>", opts) ]]
keymap("n", "<leader>eC", ":Telescope file_browser path=~/.config/<cr>", set_desk(opts, "[C]onfig dir"))
keymap("n", "<leader>en", "::Telescope file_browser path=~/Documents/notes/<cr>", set_desk(opts, "[n]otes"))
-- new in same buffer (my emacs keybinding for scratch buffer)
keymap("n", "<leader>fsB", ":enew<cr>", set_desk(opts, "[b]uffer"))
