local orgmode = require("orgmode")

-- tree sitter
-- orgmode.setup_ts_grammar()

local agend_files_dir = "~/Documents/notes/org"

-- orgmode setup
orgmode.setup({
  org_agenda_files = { agend_files_dir .. "/**/*" },
  org_default_notes_file = "~/Documents/notes/org/refile.org",
  org_todo_keywords = { "TODO(t)", "NOW(N)", "NEXT(n)", "SOMD(s)", "WAFO(w)", "|", "DONE(d)", "CANC(c)" },
  -- TODO: set colors for todo state in org
  -- org_todo_keyword_faces = {
  --   NOW = ':foreground blue :weight bold',
  --   CANC = ':background #FFFFFF :slant italic :underline on',
  --   DONE = ':background #FFFFFF :slant italic :underline on',
  --   TODO = ':background #000000 :foreground red', -- overrides builtin color for `TODO` keyword
  -- },
  org_log_done = 'time',
  org_log_into_drawer = "LOGBOOK",
  emacs_config = "~/.config/emacs/init.el",
  org_agenda_start_on_weekday = false,
  -- org_agenda_start_day = "-1d",
  org_priority_default = "C",
  org_priority_highest = "A",
  org_priority_lowest = "H",
})

local keymap = vim.keymap.set
local opts = { silent = true, noremap = true }

keymap({ "n", "v" }, "<leader>of", ":Lf " .. agend_files_dir .. "<cr>", set_desk(opts, "agenda [f]iles"))

-- TODO: add this keys
-- "o" '(:ignore t :which-key "org")
-- "oa" '(org-agenda :which-key "agenda")
-- "oc" '(cfw:open-org-calendar :which-key "caledar")
-- "oh" '(org-habit-stats-view-habit-at-point :which-key "habit stats")
-- "op" '(org-set-property :which-key "property")
-- ; todo files
-- "or" '((lambda () (interactive) (find-file "~/Documents/notes/org/refile.org")) :which-key "refile")
-- "oR" '((lambda () (interactive) (find-file "~/Documents/notes/org/repeaters.org")) :which-key "repeaters")
-- "oH" '((lambda () (interactive) (find-file "~/Documents/notes/org/habits.org")) :which-key "habits")
-- "oA" '((lambda () (interactive) (find-file "~/Documents/notes/org/asweb.org")) :which-key "habits")
-- ;; "or" '((lambda () (interactive) (find-file "~/Documents/notes/org/refile.org")) :which-key "refile")
-- ;; "oR" '((lambda () (interactive) (find-file "~/Documents/notes/org/repeaters.org")) :which-key "repeaters")
-- ;; "oh" '((lambda () (interactive) (find-file "~/Documents/notes/org/habits.org")) :which-key "habits")
-- ;; "or" '((lambda () (interactive) (switch-to-buffer "refile.org")) :which-key "refile")
-- ;; "oR" '((lambda () (interactive) (switch-to-buffer "repeaters.org")) :which-key "repeaters")
-- ;; "oH" '((lambda () (interactive) (switch-to-buffer "habits.org")) :which-key "habits")
-- ;; "oA" '((lambda () (interactive) (switch-to-buffer "asweb.org")) :which-key "work")
-- "oT" '(org-todo-list :which-key "toggle")
-- "oq" '(org-todo-list :which-key "toggle")
-- "ot" '(:ignore t :which-key "toggle")
-- "ott" '((lambda () (interactive) (org-todo)) :which-key "[T]odo")
-- "otp" '((lambda () (interactive) (org-priority)) :which-key "[P]riority")
-- "otc" '((lambda () (interactive) (org-toggle-checkbox)) :which-key "[C]heckbox")
-- "otg" '((lambda () (interactive) (org-set-tags-command)) :which-key "ta[G]")
-- "ots" '(org-schedule :which-key "[S]chedule")
-- "otd" '(org-deadline :which-key "[D]eadline")
-- "oti" '(org-insert-todo-heading-respect-content :which-key "[I]insert todo")
-- "otm" '(org-toggle-inline-images :which-key "i[M]age")
-- "ot+" '((lambda () (interactive) (org-timestamp-up-day)) :which-key "[+]day")
-- "ot-" '((lambda () (interactive) (org-timestamp-down-day)) :which-key "[-]day")

-- orgmode.action("", "")
