local status_ok, gs = pcall(require, "gitsigns")
if not status_ok then
  return
end

gs.setup {
  -- signs = {
  --   add = { hl = "GitSignsAdd", text = "▎", numhl = "GitSignsAddNr", linehl = "GitSignsAddLn" },
  --   change = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
  --   delete = { hl = "GitSignsDelete", text = "契", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
  --   topdelete = { hl = "GitSignsDelete", text = "契", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
  --   changedelete = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
  -- },
  signs = {
    add          = { text = '▎' },
    change       = { text = '▎' },
    delete       = { text = '契' },
    topdelete    = { text = '契' },
    changedelete = { text = '▎' },
    untracked    = { text = '┆' },
  },
  signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
  numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
  linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
  word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
  watch_gitdir = {
    interval = 1000,
    follow_files = true,
  },
  attach_to_untracked = true,
  current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
    delay = 1000,
    ignore_whitespace = false,
  },
  -- current_line_blame_formatter_opts = {
  --   relative_time = false,
  -- },
  sign_priority = 6,
  update_debounce = 100,
  status_formatter = nil, -- Use default
  max_file_length = 40000,
  preview_config = {
    -- Options passed to nvim_open_win
    border = "single",
    style = "minimal",
    relative = "cursor",
    row = 0,
    col = 1,
  },
  -- yadm = {
  --   enable = false,
  -- },
}

local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

-- stage 
keymap('n', 'gsh', gs.stage_hunk, set_desk(opts, "stage [h]unk"))
keymap('n', 'gsu', gs.undo_stage_hunk, set_desk(opts, "[u]ndo stage hunk"))
keymap('n', 'gsb', gs.stage_buffer, set_desk(opts, "stage [b]uffer"))
-- reset
keymap('n', 'gsr', gs.reset_hunk, set_desk(opts, "[r]eset hunk"))
keymap('n', 'gsR', gs.reset_buffer, set_desk(opts, "[R]eset buffer"))
-- diff
keymap('n', 'gsd', gs.diffthis, set_desk(opts, "[d]iff this"))
keymap('n', 'gsD', function() gs.diffthis('~') end, set_desk(opts, "[D]iff this"))
-- toggle
keymap('n', 'gstb', gs.toggle_current_line_blame, set_desk(opts, "current line [b]lame"))
keymap('n', 'gstd', gs.toggle_deleted, set_desk(opts, "[d]eleted"))
-- preview
keymap('n', 'gsp', gs.preview_hunk, set_desk(opts, "[p]review hunk"))
-- next
keymap("n", "]g", gs.next_hunk, set_desk(opts, "[g]next hunk"))
keymap("n", "[g", gs.prev_hunk, set_desk(opts, "[g]prev hunk"))
keymap("n", "gn", gs.next_hunk, set_desk(opts, "[n]ext hunk"))
keymap("n", "gN", gs.prev_hunk, set_desk(opts, "[N]prev hunk"))

keymap('n', 'gsB', function() gs.blame_line{full=true} end, set_desk(opts, "[B]lame line"))

keymap('v', 'gsh', function() gs.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end, set_desk(opts, "stage [h]unk"))
keymap('v', 'gsr', function() gs.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end, set_desk(opts, "[r]eset hunk"))

