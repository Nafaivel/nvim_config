local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

-- -- InstantMarkdown
keymap("n", "<leader>md", ":InstantMarkdownPreview <CR>", set_desk(opts, "[d]preview"))
keymap("n", "<leader>mD", ":InstantMarkdownStop <CR>:InstantMarkdownPreview <CR>", set_desk(opts, "[D]restart preview"))

vim.g["instant_markdown_slow"] = 1
vim.g["vim_markdown_new_list_item_indent"] = 2
vim.g["vim_markdown_no_extensions_in_markdown"] = 1

vim.g["instant_markdown_autostart"] = 0
vim.g["instant_markdown_autoscroll"] = 1
-- vim.g['instant_markdown_allow_unsafe_content'] = 1
-- vim.g['instant_markdown_allow_external_content'] = 0
-- vim.g['instant_markdown_mathjax'] = 1
-- vim.g['instant_markdown_mermaid'] = 1
-- vim.g['instant_markdown_python'] = 1
vim.g["instant_markdown_browser"] = "qutebrowser --target private-window"
-- vim.g['instant_markdown_browser'] = "librewolf --new-window"
-- vim.g['instant_markdown_browser'] = "flatpak run io.gitlab.librewolf-community --new-window"
