local ls = require("luasnip")
local i = ls.insert_node
local s = ls.snippet
local fmt = require("luasnip.extras.fmt").fmt

-- toki is prefix for all tokio stuff

return {
  s(
    "tokimain",
    fmt(
      [[
        #[tokio::main]
        async fn main() {{
            {}
        }}
      ]],
      { i(0)}
    )
  ),
  s(
    "tokitest",
    fmt(
      [[
        #[tokio::test]
        async fn {}() {{
            {}
        }}
      ]],
      { i(1), i(0)}
    )
  ),
}
