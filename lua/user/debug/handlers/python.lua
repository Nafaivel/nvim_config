local M = {}

local function setup()
  local ok, dappy = pcall(require, 'dap-python')
  if not ok then
    return
  end

  dappy.setup('~/.local/share/nvim/mason/packages/debugpy/venv/bin/python')
	-- local dap = require("dap")
	--
	-- local cfg = dap.configurations
	-- local adapter = dap.adapters
	--
	-- adapter.python = {
	-- 	type = "executable",
	-- 	command = "/usr/bin/python",
	-- 	args = {
	-- 		"-m",
	-- 		"debugpy.adapter",
	-- 	},
	-- }
	--
	-- cfg.python = {
	-- 	{
	-- 		type = "python",
	-- 		request = "launch",
	-- 		name = "Launch file",
	-- 		program = "${file}", -- This configuration will launch the current file if used.
	-- 	},
	-- }
end

M.setup = setup

return M
