return {
  -- My plugins here
  "nvim-lua/popup.nvim",         -- An implementation of the Popup API from vim in Neovim
  "nvim-lua/plenary.nvim",       -- Useful lua functions used ny lots of plugins
  "nvim-tree/nvim-web-devicons", -- Adds icons for other plugins
  "windwp/nvim-autopairs",       -- Autopairs, integrates with both cmp and treesitter
  "FooSoft/vim-argwrap",         -- Argwrap for nvim
  {
    "lmburns/lf.nvim",
    event = "VeryLazy",
    config = function ()
      require("user.plugins.lf")        -- file manager
    end
  },
  "numToStr/Comment.nvim",       -- Easily comment stuff
  "akinsho/bufferline.nvim",     -- Display opened buffers in statusline
  "moll/vim-bbye",               -- Close buffers
  "akinsho/toggleterm.nvim",     -- Improves interaction with terminal inside nvim
  "norcalli/nvim-colorizer.lua", -- hex colors
  "uga-rosa/ccc.nvim",           -- colorpicker
  "lervag/vimtex",               -- Latex support
  "itchyny/calendar.vim",        -- add :Calendar in vim
  "kylechui/nvim-surround",      -- brackets and other stuff
  -- "kshenoy/vim-signature", -- visualize marks id
  "https://gitlab.com/HiPhish/rainbow-delimiters.nvim",
  "folke/todo-comments.nvim", -- mark TODO HACK BUG
  "folke/which-key.nvim",     -- show keys on pres
  { dir = "~/development/lua/better-spell.nvim", },
  {
    "nvim-orgmode/orgmode",
    event = "VeryLazy",
    ft = { "org" },
  },
  {
    "nvim-orgmode/org-bullets.nvim",
    ft = { "org" },
  },
  {
    "ggandor/leap.nvim", -- buffer jump
    dependencies = { "tpope/vim-repeat" },
  },
  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  "mbbill/undotree",
  {
    "ThePrimeagen/harpoon",
    -- TODO: migrate when fixed (just uncomment in ~/.config/nvim/lua/user/plugins/harpoon.lua)
    -- branch = "harpoon2",
    dependencies = { { "nvim-lua/plenary.nvim" } },
  },
  "windwp/nvim-ts-autotag",
  "anuvyklack/pretty-fold.nvim",

  -- Telescope
  {
    "nvim-telescope/telescope.nvim",
    dependencies = { { "nvim-lua/plenary.nvim" } },
  },
  -- headers for telescope for markdown, org, neorg and more
  "crispgm/telescope-heading.nvim",
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    build =
    "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
  },
  "nvim-telescope/telescope-project.nvim",
  "nvim-telescope/telescope-file-browser.nvim",
  -- "ahmedkhalf/project.nvim",
  "smartpde/telescope-recent-files",
  {
    "nvim-telescope/telescope-frecency.nvim",
    dependencies = { "kkharji/sqlite.lua" },
  },
  {
    "2kabhishek/nerdy.nvim",
    dependencies = {
      "stevearc/dressing.nvim",
      "nvim-telescope/telescope.nvim",
    },
    cmd = "Nerdy",
  },
  "natecraddock/workspaces.nvim",

  -- Markdown
  -- use {"iamcco/markdown-preview.nvim", build = 'cd app && yarn install'} -- Add Markdown preview in nvim
  "godlygeek/tabular",
  --[[ "preservim/vim-markdown", ]]
  "ixru/nvim-markdown",
  {
    "MeanderingProgrammer/markdown.nvim",
    name = "render-markdown", -- Only needed if you have another plugin named markdown.nvim
    dependencies = { "nvim-treesitter/nvim-treesitter" },
  },
  "Myzel394/easytables.nvim",
  -- { "instant-markdown/vim-instant-markdown", build = "yarn install" },
  {
    dir = "~/development/lua/md-composer.nvim/",
    cmd = { "ComposerOpen", "ComposerJob", "ComposerUpdate", "ComposerStart" },
    build = "cargo build --release",
    ft = { "markdown" },
  },
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    build = "cd app && yarn install",
    init = function()
      vim.g.mkdp_filetypes = { "markdown" }
    end,
    ft = { "markdown" },
  },

  -- Translate
  --[[ "voldikss/vim-translator", ]]
  "uga-rosa/translate.nvim",

  -- Nvim todo.txt highlight
  --[[ use {"https://gitlab.com/Nafaivel/nvim-todo.txt"} ]]
  { dir = "~/development/lua/nvim-todo.txt" },

  -- colorschemes
  {
    "rktjmp/lush.nvim", --req bloop
    "nocksock/bloop.nvim",
    "bluz71/vim-moonfly-colors",
    "EdenEast/nightfox.nvim",
    "Mofiqul/dracula.nvim",
    "Everblush/everblush.vim",
    "marko-cerovac/material.nvim",
    "rebelot/kanagawa.nvim",
    "cooperuser/glowbeam.nvim",
    "patstockwell/vim-monokai-tasty",
    "NTBBloodbath/doom-one.nvim",
    "Shatur/neovim-ayu",
    "catppuccin/nvim",
    "nyoom-engineering/oxocarbon.nvim",
    "fynnfluegge/monet.nvim",
    "ribru17/bamboo.nvim",
    "olimorris/onedarkpro.nvim",
    'Verf/deepwhite.nvim',
  },

  -- dashboard
  "glepnir/dashboard-nvim",

  -- {
  --   "nomnivore/ollama.nvim",
  --   dependencies = {
  --     "nvim-lua/plenary.nvim",
  --   },
  --   -- All the user commands added by the plugin
  --   cmd = { "Ollama", "OllamaModel", "OllamaServe", "OllamaServeStop" },
  -- },

  { "David-Kunz/gen.nvim" },

  -- StatusLine
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons", lazy = true },
  },

  -- cmp plugins
  -- "hrsh7th/nvim-cmp",        -- The completion plugin
  -- "hrsh7th/cmp-buffer",      -- buffer completions
  -- "hrsh7th/cmp-path",        -- path completions
  -- "hrsh7th/cmp-cmdline",     -- cmdline completions
  -- "saadparwaiz1/cmp_luasnip", -- snippet completions
  -- "hrsh7th/cmp-nvim-lsp",    -- LSP integration
  -- "hrsh7th/cmp-nvim-lua",    -- lua nvim config completions

  -- cmp plugins
  { "iguanacucumber/magazine.nvim", name = "nvim-cmp" },                -- cmp itself
  { "iguanacucumber/mag-buffer",    name = "cmp-buffer" },              -- buffer completions
  { "iguanacucumber/mag-cmdline",   name = "cmp-cmdline" },             -- cmdline completions
  { "iguanacucumber/mag-nvim-lsp",  name = "cmp-nvim-lsp", opts = {} }, -- LSP integration
  { "iguanacucumber/mag-nvim-lua",  name = "cmp-nvim-lua" },            -- lua nvim config completions
  { "saadparwaiz1/cmp_luasnip" },                                       -- snippet completions
  { "hrsh7th/cmp-path" },                                               -- path completions

  -- rust
  {
    "mrcjkb/rustaceanvim",
    -- version = "5.4.2",
    ft = "rust",
    lazy = false,
    dependencies = {
      "nvim-lua/plenary.nvim",
      "mfussenegger/nvim-dap",
    },
  },
  { 'saecki/crates.nvim', tag = 'stable', },
  -- go
  {
    "ray-x/go.nvim",
    dependencies = { -- optional packages
      "ray-x/guihua.lua",
      "neovim/nvim-lspconfig",
      "nvim-treesitter/nvim-treesitter",
    },
    config = function()
      require("go").setup()
    end,
    event = { "CmdlineEnter" },
    ft = { "go", "gomod" },
  },
  -- yaml
  {
    "cuducos/yaml.nvim",
    ft = "yaml", -- optional
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-telescope/telescope.nvim", -- optional
    },
  },
  "pedrohdz/vim-yaml-folds",

  -- useage of project-local configuration files
  "folke/neoconf.nvim",
  -- setup poetry venv for python
  {
    "rafi/neoconf-venom.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    version = false,
    ft = "python",
  },
  {
    "meatballs/notebook.nvim",
    -- ft = "python,ipynb",
    config = function()
      require("notebook").setup({
        -- Whether to insert a blank line at the top of the notebook
        insert_blank_line = true,

        -- Whether to display the index number of a cell
        show_index = true,

        -- Whether to display the type of a cell
        show_cell_type = true,

        -- Style for the virtual text at the top of a cell
        virtual_text_style = { fg = "lightblue", italic = true },
      })
    end,
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
  },
  -- { "luk400/vim-jukit",  ft = "ipynb" },

  -- snippets
  "L3MON4D3/LuaSnip",             --snippet engine
  "rafamadriz/friendly-snippets", -- a bunch of snippets to use

  -- Lsp installer (Mason)
  {
    "williamboman/mason.nvim",
    "RubixDev/mason-update-all",
    "williamboman/mason-lspconfig.nvim",
    "zapling/mason-conform.nvim",
    "jayp0521/mason-nvim-dap.nvim",
  },

  -- LSP
  { "folke/neodev.nvim",  opts = {} },
  "stevearc/conform.nvim",
  "neovim/nvim-lspconfig",    -- enable LSP
  "ray-x/lsp_signature.nvim", -- show function signatures
  "elkowar/yuck.vim",         -- yuck ft support (widgets for eww)
  {
    "rachartier/tiny-code-action.nvim",
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      { "nvim-telescope/telescope.nvim" },
    },
    event = "LspAttach",
    config = function()
      require("tiny-code-action").setup()
    end,
  },
  {
    "j-hui/fidget.nvim",
    opts = {},
  },

  -- Debuger
  "mfussenegger/nvim-dap",
  { "rcarriga/nvim-dap-ui", dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" } },
  "mfussenegger/nvim-dap-python",

  -- Treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
  },
  "JoosepAlviste/nvim-ts-context-commentstring",

  -- Git
  "lewis6991/gitsigns.nvim",
  "tpope/vim-fugitive",
}
