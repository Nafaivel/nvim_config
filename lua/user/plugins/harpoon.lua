local ok, harpoon = pcall(require, "harpoon")

if not ok then
  return
end

harpoon.setup({tabline=false})

local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

Tabline = false
vim.opt.showtabline = 0

keymap("n", "<leader>hm", ":lua require('harpoon.mark').add_file()<cr>", set_desk(opts, "[m]ark buffer"))
keymap("n", "<leader>hd", ":lua require('harpoon.mark').rm_file()<cr>", set_desk(opts, "[d]elete/unmark buffer"))
keymap("n", "<leader>hq", ":lua require('harpoon.ui').toggle_quick_menu()<cr>", set_desk(opts, "[q]uick menu"))
keymap("n", "<leader>ht", function()
  Tabline = not Tabline
  if Tabline then
    vim.opt.showtabline = 2
  else
    vim.opt.showtabline = 0
  end
end, set_desk(opts, "[t]abline"))

keymap("n", "<C-H>", ":lua require('harpoon.ui').nav_prev()<cr>", set_desk(opts, "[h]next"))
keymap("n", "<C-L>", ":lua require('harpoon.ui').nav_next()<cr>", set_desk(opts, "[l]prev"))

for i = 1, 9, 1 do
  keymap("n", "<leader>h"..i, ":lua require('harpoon.ui').nav_file("..i..")<cr>" , set_desk(opts, "["..i.."] go to"))
end

-- -- harpoon:setup()
-- harpoon:setup({
--     settings = {
--         save_on_toggle = true,
--         sync_on_ui_close = true,
--     }
-- })
--
-- local keymap = vim.keymap.set
-- local opts = { noremap = true, silent = true }
--
-- keymap("n", "<leader>hm", function() harpoon:list():add() end, set_desk(opts, "[m]ark buffer"))
-- keymap("n", "<leader>hq", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end, set_desk(opts, "[q]uick menu"))
--
-- -- keymap("n", "H", function() harpoon:list():prev() end, set_desk(opts, "[H]prev"))
-- -- keymap("n", "L", function() harpoon:list():next() end, set_desk(opts, "[L]next"))
-- keymap("n", "<C-H>", function() harpoon:list():prev() end, set_desk(opts, "[H]prev"))
-- keymap("n", "<C-L>", function() harpoon:list():next() end, set_desk(opts, "[L]next"))
--
-- for i = 1, 9, 1 do
-- 	keymap("n", "<leader>h" .. i, function() harpoon:list():select(i) end, set_desk(opts, "[" .. i .. "] go to"))
-- end
