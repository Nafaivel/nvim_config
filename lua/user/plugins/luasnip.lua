local status_ok, ls = pcall(require, "luasnip")
if not status_ok then
  return
end
local types = require("luasnip.util.types")

-- Every unspecified option will be set to the default.
ls.config.set_config({
  history = true,
  -- Update more often, :h events for more info.
  updateevents = "TextChanged,TextChangedI",
  ext_opts = {
    [types.choiceNode] = {
      active = {
        virt_text = { { "choiceNode", "Comment" } },
      },
    },
  },
  -- treesitter-hl has 100, use something higher (default is 200).
  ext_base_prio = 300,
  -- minimal increase in priority.
  ext_prio_increase = 1,
  enable_autosnippets = true,
  store_selection_keys = "<Tab>",
})

-- Function to reload snippets
local function reload_snippets()
  require("luasnip.loaders.from_lua").load({
    paths = { "~/.config/nvim/snippets/" },
    fs_event_providers = {
      autocmd = true,
      libuv = true,
    },
  })
end

vim.keymap.set("n", "<leader><leader>s", reload_snippets, { noremap = true, silent = true })
