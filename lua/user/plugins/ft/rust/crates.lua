local opts = { silent = true, noremap = true, buffer = true }
local keymap = vim.keymap.set

vim.api.nvim_create_autocmd("BufRead", {
  pattern = "Cargo.toml",
  callback = function()
    local ok, cr = pcall(require, "crates")
    if not ok then
      return
    end

    cr.setup({})

    keymap("n", "<leader>llct", cr.toggle, set_desk(opts, "[t]oggle"))
    keymap("n", "<leader>llcr", cr.reload, set_desk(opts, "[r]eload"))

    keymap("n", "<leader>llcv", cr.show_versions_popup, set_desk(opts, "[v]ersions"))
    keymap("n", "<leader>llcf", cr.show_features_popup, set_desk(opts, "[f]eatures"))
    keymap("n", "<leader>llcd", cr.show_dependencies_popup, set_desk(opts, "[d]ependencies"))

    keymap("n", "<leader>llcu", cr.update_crate, set_desk(opts, "[u]pdate crate"))
    keymap("v", "<leader>llcu", cr.update_crates, set_desk(opts, "[u]pdate crates"))
    keymap("n", "<leader>llca", cr.update_all_crates, set_desk(opts, "[u]pdate all crates"))
    keymap("n", "<leader>llcU", cr.upgrade_crate, set_desk(opts, "[U]pgrade crate"))
    keymap("v", "<leader>llcU", cr.upgrade_crates, set_desk(opts, "[U]pgrade crates"))
    keymap("n", "<leader>llcA", cr.upgrade_all_crates, set_desk(opts, "[U]pgrade all crates"))

    keymap("n", "<leader>llcx", cr.expand_plain_crate_to_inline_table, set_desk(opts, "expand"))
    keymap("n", "<leader>llcX", cr.extract_crate_into_table, set_desk(opts, "extract_crate_into_table"))

    keymap("n", "<leader>llcH", cr.open_homepage, set_desk(opts, "crate [H]omepage"))
    keymap("n", "<leader>llcR", cr.open_repository, set_desk(opts, "crate [R]epo"))
    keymap("n", "<leader>llcD", cr.open_documentation, set_desk(opts, "crate [D]oc"))
    keymap("n", "<leader>llcC", cr.open_crates_io, set_desk(opts, "[C]rates.io"))
    keymap("n", "<leader>llcL", cr.open_lib_rs, set_desk(opts, "[L]ib.rs"))


    local function show_documentation()
      local filetype = vim.bo.filetype
      if filetype == "vim" or filetype == "help" then
        vim.cmd('h ' .. vim.fn.expand('<cword>'))
      elseif filetype == "man" then
        vim.cmd('Man ' .. vim.fn.expand('<cword>'))
      elseif require('crates').popup_available() then
        require('crates').show_popup()
      else
        vim.lsp.buf.hover()
      end
    end

    vim.keymap.set('n', 'K', show_documentation, { silent = true })
  end
})
