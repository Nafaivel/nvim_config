local ls = require("luasnip")
local i = ls.insert_node
local s = ls.snippet
local rep = require("luasnip.extras").rep
local fmt = require("luasnip.extras.fmt").fmt

return {
  s(
    "reqf",
    fmt(
      [[
        local {} = require("{}").{}
      ]],
      { rep(2), i(1, "module_name"), i(2, "function_name") }
    )
  ),
  s(
    "pcall",
    fmt(
      [[
        local ok, {} = pcall(require, "{}")
        if not ok then
          return
        end

        {}
      ]],
      { i(1, "var"), i(2, ""), i(0) }
    )
  ),
}
