local status_ok, leap = pcall(require, "leap")
if not status_ok then
  return
end

local keymap = vim.keymap.set

keymap({ "n", "x", "o" }, "<leader>/", "<Plug>(leap-forward)", { desc = "[/]quick forvard" })
keymap({ "n", "x", "o" }, "<leader>?", "<Plug>(leap-backward)", { desc = "[?]quick backward" })
keymap({ "n", "x", "o" }, "<leader>g/", "<Plug>(leap-from-window)", { desc = "[/]leap window" })

leap.opts = {
  case_sensitive = false,
  equivalence_classes = { " 	" },
  max_phase_one_targets = nil,
  highlight_unlabeled_phase_one_targets = false,
  max_highlighted_traversal_targets = 10,
  substitute_chars = {},
  safe_labels = "sfnut/SFNLHMUGTZ?",
  labels = "sfnjklhodweimbuyvrgtaqpcxz/SFNJKLHODWEIMBUYVRGTAQPCXZ?",
  special_keys = {
    next_target = "<enter>",
    prev_target = "<tab>",
    next_group = "<space>",
    prev_group = "<tab>",
  },
}
