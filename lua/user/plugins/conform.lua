local c_ok, conform = pcall(require, "conform")
if not c_ok then
  print("conform not work")
  return
end

conform.setup({
  default_format_opts = { lsp_format = "fallback" },
  formatters_by_ft = {
    lua = { lsp_format = "prefer" },
    -- Conform will run multiple formatters sequentially
    -- python = { "isort", "black" },
    -- You can customize some of the format options for the filetype (:help conform.format)
    rust = { lsp_format = "prefer" },
    -- Conform will run the first available formatter
    -- javascript = { "prettierd", "prettier", stop_after_first = true },
  },
})

local mc_ok, mc = pcall(require, "mason-conform")
if not mc_ok then
  print("conform without mason not work")
  return
end
mc.setup({
  ignore_install = { 'prettier' } -- List of formatters to ignore during install
})

local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

keymap({ "n", "v" }, "<leader>lf", function()
  vim.lsp.buf.format()
  conform.format()
end, set_desk(opts, "[f]ormat"))
