local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end
require "user.lsp.filetypes" -- setup filetypes for lspconfig

require "user.lsp.mason"

require("user.lsp.handlers").setup()

local cfg = {
  bind = true,
  hint_enable = true,
  handler_opts = {
    border = "shadow" -- double, rounded, single, shadow, none
  },
  -- shadow_blend = 36, -- if you using shadow as border use this set the opacity
  shadow_guibg = "#ffffff", -- if you using shadow as border use this set the color e.g. 'Green' or '#121315'

}                           -- add you config here
require("lsp_signature").setup(cfg)

-- rename keybinding
vim.api.nvim_set_keymap("n", "<leader>lr", "<cmd>lua vim.lsp.buf.rename()<CR>", { noremap = true, desc = "[r]ename" })
vim.api.nvim_set_keymap("n", "<leader>lq", "<cmd>LspRestart<CR>", { noremap = true, desc = "[q]restart" })
vim.api.nvim_set_keymap("n", "<leader>lQ", "<cmd>LspStop<CR>", { noremap = true, desc = "[Q]stop" })
