local ls = require("luasnip")
local s = ls.snippet
-- local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
-- local c = ls.choice_node
-- local d = ls.dynamic_node
-- local l = require("luasnip.extras").lambda
-- local r = require("luasnip.extras").rep
-- local p = require("luasnip.extras").partial
-- local m = require("luasnip.extras").match
-- local n = require("luasnip.extras").nonempty
-- local dl = require("luasnip.extras").dynamic_lambda
-- local fmt = require("luasnip.extras.fmt").fmt
-- local fmta = require("luasnip.extras.fmt").fmta
-- local types = require("luasnip.util.types")
-- local conds = require("luasnip.extras.expand_conditions")
-- local events = require("luasnip.util.events")

local trim_date = true

--- @return string
local function get_filename()
	local filepath = vim.api.nvim_buf_get_name(0)
	local pattern = "(.*)/(.+)$"
  -- trim all except filename
	local filename = filepath:gsub(pattern, "%2")
  -- trim extension
  filename = filename:match("(.+)%..+$")
	return filename
end

--- @return string
local function get_kastler_filename()
  local filename = get_filename()
  if trim_date then
    filename = string.sub(filename, 10,-1)
  end
  filename = filename:gsub("_", " ")
  return filename
end

return {
	s({ trig = "khead" }, {
		t({ "# " }),
		f(get_kastler_filename),
		i(1),
		t({ "", "", "" }),
		i(0),
	}),
	s({ trig = "head" }, {
		t({ "# " }),
		f(get_filename),
		i(1),
		t({ "", "" }),
		i(0),
	}),
}
