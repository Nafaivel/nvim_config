require("user.options") -- nvim options
require("user.utils")   -- helper functions
require("user.keymaps") -- setup part of keymaps other mostly in plugins files
require("user.aliases") -- functions aliases
require("user.spell")   -- setup spellcheck
require("user.plugins") -- install and configure plugins
require("user.colors")  -- colorschemes and etc.
require("user.neoconf") -- project-local configuration files
require("user.nvimdev") -- enables lsp hints for nvim
require("user.lsp")     -- setup lsp
require("user.debug")   -- setup dap
require("user.todotxt") -- todotxt
require("user.pgdev")   -- plugin development helpers
