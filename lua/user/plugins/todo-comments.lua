local ok, tc = pcall(require, "todo-comments")
if not ok then
  return
end

local cfg = {
  keywords = {
    FIX = {
      icon = " ", -- icon used for the sign, and in search results
      color = "error", -- can be a hex color, or a named color (see below)
      alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
      -- signs = false, -- configure signs for some keywords individually
    },
    TODO = { icon = " ", color = "info", alt = { "todo" } },
    HACK = { icon = " ", color = "warning" },
    WARN = { icon = " ", color = "warning", alt = { "WARNING", "XXX" } },
    PERF = { icon = " ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    NOTE = { icon = " ", color = "hint", alt = { "INFO" } },
    TEST = { icon = "⏲ ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
    ASK = { icon = " ", color = "info" },
    THINK = { icon = "󰧑 ", color = "info", alt = { "REFLECT" } },
  },
  merge_keywords = true,
}

tc.setup(cfg)

local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set
keymap("n", "<leader>ft", ":TodoTelescope<CR>", set_desk(opts, "[t]odo"))
keymap("n", "]t", tc.jump_next, set_desk(opts, "[t]odo"))
keymap("n", "[t", tc.jump_prev, set_desk(opts, "[t]odo"))
