local function escape(str)
  -- You need to escape these characters to work correctly
  local escape_chars = [[;,."|\]]
  return vim.fn.escape(str, escape_chars)
end

-- Recommended to use lua template string
local en = [[`qwertyuiop[]asdfghjkl;'zxcvbnm]]
local be = [[ёйцукенгшўзхъфывапролджэячсміть]]
local en_shift = [[~QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>]]
local be_shift = [[ËЙЦУКЕНГШЎЗХЪФЫВАПРОЛДЖЭЯЧСМІТЬБЮ]]

return vim.fn.join({
    -- | `to` should be first     | `from` should be second
    escape(be_shift) .. ';' .. escape(en_shift),
    escape(be) .. ';' .. escape(en),
}, ',')

