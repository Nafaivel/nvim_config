return {
	parse("isodate", os.date("%Y%m%d")),
	parse("today", os.date("%Y-%m-%d ")),
	parse("tomorrow", os.date("%Y-%m-%d ", os.time()+24*60*60)),
}
