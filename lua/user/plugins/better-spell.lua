local is_ok, bs = pcall(require, "better-spell")

if not is_ok then
  return
end

bs.setup({
  dicts = { "en_US", "be_BY", "be_BY@tarask" },
  output_type = "floating",
})

vim.keymap.set({ "n", "v" }, "<leader>Sk", BS.check_under_cursor)
vim.keymap.set({ "n", "v" }, "<leader>K", function()
  BS.check_under_cursor(true)
end)
vim.keymap.set({ "n", "v" }, "<leader>Sb", BS.check_buffer)
vim.keymap.set({ "n", "v" }, "<leader>Sc", function()
  BS.clear_hl()
end)
-- vim.keymap.set({ "n", "v" }, "<leader>m", ":messages<cr>")
