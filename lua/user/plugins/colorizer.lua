local function setup_colorizer()
  local ok, col = pcall(require, "colorizer")
  if not ok then
    return
  end

  col.setup(
  -- per mode
    {
      '*',                  -- Highlight all files, but customize some others.
      css = { css = true }, -- Enable parsing rgb(...) functions in css.
    },
    -- defaults for others
    { names = false })
end

local function setup_ccc()
  local ok_, ccc = pcall(require, "ccc")
  if not ok_ then
    return
  end

  local output = ccc.output

  ccc.setup({
    outputs = {
      output.hex,
      output.css_rgba,
      output.css_rgb,
      output.css_hsl,
    },
  })

  local keymap = vim.keymap.set
  local opts = { noremap = true, silent = true }

  keymap("n", "<leader>lcp", ":CccPick<cr>", set_desk(opts, "color picker"))
end

setup_colorizer()
setup_ccc()
