local status_ok, bufferline = pcall(require, "bufferline")
if not status_ok then
  return
end

local filtered_filetypes = { "help", "netrw" }

---comment
---@param input string
---@param list Array<string>
---@return boolean
local function matches_any_variant(input, list)
  -- print(input)
  for _, variant in ipairs(list) do
    if input == variant then
      print(input)
      return true
    end
  end
  return false
end

local opts = {
  numbers = "none", -- | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
  close_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
  right_mouse_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
  left_mouse_command = "buffer %d", -- can be a string | function, see "Mouse actions"
  middle_mouse_command = nil, -- can be a string | function, see "Mouse actions"
  buffer_close_icon = "󰛉", -- "",
  modified_icon = "●",
  close_icon = "",
  truncate_names = true, -- whether or not tab names should be truncated
  left_trunc_marker = "",
  right_trunc_marker = "",
  max_name_length = 30,
  max_prefix_length = 30,   -- prefix used when a buffer is de-duplicated
  tab_size = 21,
  diagnostics = "nvim_lsp", -- | "nvim_lsp" | "coc",
  diagnostics_update_in_insert = false,
  offsets = { { filetype = "NvimTree", text = "", padding = 1 } },
  show_buffer_icons = true,
  show_buffer_close_icons = true,
  show_close_icon = true,
  show_tab_indicators = true,
  persist_buffer_sort = false, -- whether or not custom sorted buffers should persist
  separator_style = "thin",    -- | "thick" | "thin" | { 'any', 'any' },
  enforce_regular_tabs = true,
  always_show_bufferline = true,
  groups = require("user.plugins.bufferline.groups"),
  highlights = require("user.plugins.bufferline.highlights"),
  custom_filter = function(buf, _)
    if matches_any_variant(vim.bo[buf].filetype, filtered_filetypes) then
      print(vim.bo[buf].filetype)
      return false
    else
      return true
    end

    -- you can use more custom logic for example
    -- don't show files matching a pattern
    -- return not vim.fn.bufname(buf):match('test')

    -- show only certain filetypes in certain tabs e.g. js in one, css in
    -- another etc.
    -- local tab_num = vim.fn.tabpagenr()
    -- if tab_num == 1 and vim.bo[buf].filetype == "javascript" then
    --   return true
    -- elseif tab_num == 2 and vim.bo[buf].filetype == "css" then
    --   return true
    -- else
    --   return false
    -- end
    --
    --
    -- -- My personal example:
    -- -- Don't show output log buffers in the same tab as my other code.
    -- -- 1. Check if there are any log buffers in the full list of buffers
    -- -- if not don't do any filtering
    -- local logs =
    -- vim.tbl_filter(
    --   function(b)
    --     return vim.bo[b].filetype == "log"
    --   end,
    --   buf_nums
    -- )
    -- if vim.tbl_isempty(logs) then
    --   return true
    -- end
    -- -- 2. if there are log buffers then only show the log buffer
    -- local tab_num = vim.fn.tabpagenr()
    -- local is_log = vim.bo[buf].filetype == "log"
    -- -- only show log buffers in secondary tabs
    -- return (tab_num == 2 and is_log) or (tab_num ~= 2 and not is_log)
  end

}

bufferline.setup({
  options = opts,
})

opts = { noremap = true, silent = true }
local keymap = vim.api.nvim_set_keymap

keymap("n", "<S-l>", ":BufferLineCycleNext<CR>", opts)
keymap("n", "<S-h>", ":BufferLineCyclePrev<CR>", opts)

keymap("n", "gL", ":BufferLineMoveNext<CR>", opts)
keymap("n", "gH", ":BufferLineMovePrev<CR>", opts)

local c = require("user.colors")
c.CfgColorscheme()
c.Hl()
