local ls = require("luasnip")
local i = ls.insert_node
local s = ls.snippet
local fmt = require("luasnip.extras.fmt").fmt

return {
  -- te for thiserror
  s(
    "terr",
    fmt(
      [[
        #[derive(Debug, thiserror::Error)]
        pub enum {} {{
            {}
        }}
      ]],
      { i(1, "NameErr"), i(2, "ErrorVariant") }
    )
  ),
  s(
    "te",
    fmt(
      [[
        #[error("{}")]
        {},
      ]],
      { i(1, "Error desc"), i(2, "ErrorVariant") }
    )
  ),
  s(
    "tefr",
    fmt(
      [[
        #[error("{}")]
        {}(#[from] {}),
      ]],
      { i(1, "Error desc"), i(2, "ErrorVariant"), i(3, "FormWhat") }
    )
  ),
  -- se for snafu
  s(
    "serr",
    fmt(
      [[
        #[derive(Debug, snafu::Snafu)]
        pub enum {} {{
            {}
        }}
      ]],
      { i(1, "NameErr"), i(2, "ErrorVariant") }
    )
  ),
  s(
    "se",
    fmt(
      [[
        #[snafu(display("{}"))]
        {},
      ]],
      { i(1, "error desc"), i(2, "ErrorVariant") }
    )
  ),
  s(
    "modtest",
    fmt(
      [[
        #[cfg(test)]
        mod tests {{
            {}
        }}
      ]],
      i(0)
    )
  ),
}
