return {
  settings = {

    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = "LuaJIT",
      },
      diagnostics = {
        globals = {
          "vim",
          "awesome",
          "client",
          "mouse",
          "mousegrabber",
          "root",
          "screen",
          "tag"
        },
      },
      workspace = {
        library = {
          [vim.fn.expand("$VIMRUNTIME/lua")] = true,
          --[[ [vim.fn.stdpath("config") .. "/lua"] = true, ]]
          -- [vim.fn.expand(os.getenv("HOME") .. "/development/workflow/awesome-emmylua/lua")] = true,
        },
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}
