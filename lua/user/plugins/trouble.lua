-- Lua
local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

keymap("n", "<leader>lee", function()
  require("trouble").toggle({
    mode = "diagnostics", -- inherit from diagnostics mode
    filter = {
      any = {
        buf = 0,                               -- current buffer
        {
          severity = vim.diagnostic.severity.ERROR, -- errors only
          -- limit to files in the current project
          function(item)
            return item.filename:find((vim.loop or vim.uv).cwd(), 1, true)
          end,
        },
      },
    },
  })
end, set_desk(opts, "[e]rror"))
keymap("n", "<leader>lew", function() require("trouble").toggle({ mode = "diagnostics", }) end, set_desk(opts, "[w]orkspace"))
keymap("n", "<leader>led", function()
  require("trouble").toggle({
      mode = "diagnostics", -- inherit from diagnostics mode
      filter = { buf = 0 }, -- filter diagnostics to the current buffer
  })
end, set_desk(opts, "[d]ocument"))
keymap("n", "<leader>leq", function() require("trouble").toggle("quickfix") end, set_desk(opts, "[q]uickfix"))
keymap("n", "<leader>lel", function() require("trouble").toggle("loclist") end, set_desk(opts, "[l]oclist"))
keymap("n", "gR", function() require("trouble").toggle("lsp_references") end, set_desk(opts, "[R]eferences (trouble)"))
