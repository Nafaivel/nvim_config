local ok, wk = pcall(require, "which-key")

if not ok then
  return
end

wk.setup({
  delay = function()
    --  To make `<Leader>d` and `<Leader>dd` keymaps work
    --  make sure that opts.delay >= timeoutlen.
    --  https://github.com/folke/which-key.nvim/issues/648#issuecomment-2226881346
    local delay = 200
    return delay < vim.o.timeoutlen and vim.o.timeoutlen or delay
  end,
})

wk.add({
  {
    mode = { "n" },
    { "<leader>O",  group = "[O]llama" },
    { "<leader>e",  group = "[e]dit" },
    { "<leader>f",  group = "[f]ind" },
    { "<leader>g",  group = "[g]o" },
    { "<leader>h",  group = "[h]arpoon" },
    { "<leader>l",  group = "[l]sp" },
    { "<leader>ld", group = "[d]ap" },
    { "<leader>le", group = "[e]rrors" },
    { "<leader>ll", group = "[l]atex" },
    { "<leader>m",  group = "[m]arkdown" },
    { "<leader>o",  group = "[o]rg" },
    { "<leader>s",  group = "[s]et/source" },
    { "<leader>sT", group = "[T]ablen" },
    { "<leader>sf", group = "[f]iletype" },
  },
  {
    mode = { "v", "n" },
    { "<leader>t", group = "[t]translate" },
    { "gs",        group = "git [s]igns" },
    { "gst",       group = "[t]oggle" },
    { "gsf",       group = "[f]ugitive" },
  },
})
