local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }


vim.g["markdown_composer_browser"] = "qutebrowser --target private-window "
-- vim.g["markdown_composer_external_renderer"] = "pandoc -f markdown -t html"
-- vim.g["markdown_composer_refresh_rate"] = 100
vim.g["markdown_composer_address"] = "127.0.0.1"
vim.g["markdown_composer_port"] = 7879
vim.g["markdown_composer_autostart"] = 0
vim.g["markdown_composer_open_browser"] = 0
keymap("n", "<leader>md", ":ComposerStart<CR>:lua spawn_private_qute()<cr>", set_desk(opts, "[d]preview"))
keymap("n", "<leader>mD", ":lua kill_composer()<cr>:unlet s:job<cr>:ComposerStart<CR>", set_desk(opts, "[D]restart preview"))

function kill_composer() 
  spawn("killall", {
    args = {
      "markdown-composer"
    },
  }, {
    stdout = function() end,
    stderr = print
  }, function(code) -- we want to call this function when the process is done
    print("child process exited with code " .. string.format("%d", code))
  end)
end

function spawn_private_qute()
  spawn("qutebrowser", {
    args = {
      "--target",
      "private-window",
      "http://127.0.0.1:7879"
    },
  }, {
    stdout = function() end,
    stderr = print
  }, function(code) -- we want to call this function when the process is done
    print("child process exited with code " .. string.format("%d", code))
  end)
end
