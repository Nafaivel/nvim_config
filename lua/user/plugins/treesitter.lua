local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
  return
end

configs.setup {
  ensure_installed = {
    "rust",
    "python",
    "c_sharp",
    "html",
    "css",
    "lua",
    "markdown",
    "markdown_inline",
    "vimdoc",
    "c",
    "zig"
  },                       -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  sync_install = false,    -- install languages synchronously (only applied to `ensure_installed`)
  ignore_install = { "" }, -- List of parsers to ignore installing
  autopairs = {
    enable = true,
  },
  highlight = {
    enable = true,    -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled
    -- additional_vim_regex_highlighting = {"org", "markdown"},
    additional_vim_regex_highlighting = false,
  },
  indent = { enable = true },
  -- indent = { enable = true, disable = { "yaml", "Markdown", "Rust" } },
}


local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.org = {
  install_info = {
    url = 'https://github.com/milisims/tree-sitter-org',
    revision = 'main',
    files = { 'src/parser.c', 'src/scanner.c' },
  },
  filetype = 'org',
}
