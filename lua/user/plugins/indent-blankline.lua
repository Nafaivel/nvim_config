local ok, ibl = pcall(require, "ibl")
if not ok then
  return
end

local ibl_enabled = false
local allways_enabled_filetypes = { "yaml", "html", "python", "json" }
ibl.setup({ enabled = true })

-- toggle
local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

keymap("n", "<leader>si", function()
  ibl_enabled = not ibl_enabled
  -- ibl.update({ enabled = ibl_enabled })
end, set_desk(opts, "toggle [i]nstant breakline"))

local hooks = require("ibl.hooks")
hooks.register(hooks.type.ACTIVE, function(bufnr)
  local is_correct_ft = vim.tbl_contains(
    allways_enabled_filetypes,
    vim.api.nvim_get_option_value("filetype", { buf = bufnr })
  )
  return is_correct_ft or ibl_enabled
end)
