return {
  -- todo.txt
  -- due
	parse("dt", "due:"..os.date("%Y-%m-%d ")),
	parse("dtm", "due:"..os.date("%Y-%m-%d ", os.time()+24*60*60)),
	parse("dw", "due:"..os.date("%Y-%m-%d ", os.time()+7*24*60*60)),
	parse("dnw", "due:"..os.date("%Y-%m-%d ", os.time()+2*7*24*60*60)),
  -- complete
	parse("xt", "x " .. os.date("%Y-%m-%d ")),
	parse("xy", "x " .. os.date("%Y-%m-%d ", os.time()-24*60*60)),
  -- create task
	parse("a", "(A) " .. os.date("%Y-%m-%d ")),
	parse("b", "(B) " .. os.date("%Y-%m-%d ")),
	parse("c", "(C) " .. os.date("%Y-%m-%d ")),
	parse("d", "(D) " .. os.date("%Y-%m-%d ")),
}
