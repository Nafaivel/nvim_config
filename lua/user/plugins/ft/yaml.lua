local ok, yaml = pcall(require, "yaml_nvim")
if not ok then
  return
end

yaml.setup({ ft = { "yaml", "other yaml filetype" } })
