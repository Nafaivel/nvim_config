local opts = { noremap = true, silent = true }
local keymap = vim.keymap.set

keymap("n", "<leader>gf", ":Git<CR>", set_desk(opts, "[g]it"))
keymap("n", "<leader>gc", ":Git commit<CR>", set_desk(opts, "[c]ommit"))
keymap("n", "<leader>gl", ":Git log<CR>", set_desk(opts, "[l]og"))
keymap("n", "<leader>gw", ":Gwrite<CR>", set_desk(opts, "[w]rite"))
keymap("n", "<leader>gd", ":Gdiffsplit<CR>", set_desk(opts, "[d]iff split"))
keymap("n", "<leader>gB", ":Telescope git_branches<CR>", set_desk(opts, "[B]ranch"))
keymap({ "n", "v" }, "<leader>gb", ":Git blame<CR>", set_desk(opts, "[b]lame"))
